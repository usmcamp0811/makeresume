# MakeResume

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mcamp.gitlab.io/MakeResume.jl/dev)
[![Build Status](https://github.com/mcamp/MakeResume.jl/badges/main/pipeline.svg)](https://github.com/mcamp/MakeResume.jl/pipelines)
[![Coverage](https://github.com/mcamp/MakeResume.jl/badges/main/coverage.svg)](https://github.com/mcamp/MakeResume.jl/commits/main)
[![Coverage](https://codecov.io/gh/mcamp/MakeResume.jl/branch/main/graph/badge.svg)](https://codecov.io/gh/mcamp/MakeResume.jl)
