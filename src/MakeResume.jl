module MakeResume

using JSON
using PyCall

function __init__()
  py"""
  import jinja2
  """
end

const resume_jinja = joinpath(@__DIR__(),"resume.jinja")
const resume_tex = joinpath(@__DIR__(),"resume.tex")

function make_tex(json_path::String)
  Template = py"jinja2.Template"
  resume = JSON.parse(open(json_path))
  resume_template = read(open(resume_jinja),String)
  return Template(resume_template).render(resume)
end

function make_resume(resume_json; output_path=joinpath(pwd(),"resume.pdf"))
  cd(@__DIR__())
  resume = make_tex(resume_json)
  write(resume_tex, resume)
  @assert stat(output_path).size ≤ 0 "File already exists"
  run(`pdflatex resume.tex`)
  mv("resume.pdf", output_path)
  # some cleanup
  rm("resume.log")
  rm("resume.aux")
end

export make_tex, make_resume
end
