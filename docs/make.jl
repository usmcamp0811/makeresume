using MakeResume
using Documenter

DocMeta.setdocmeta!(MakeResume, :DocTestSetup, :(using MakeResume); recursive=true)

makedocs(;
    modules=[MakeResume],
    authors="Matthew Camp",
    repo="https://github.com/mcamp/MakeResume.jl/blob/{commit}{path}#{line}",
    sitename="MakeResume.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mcamp.gitlab.io/MakeResume.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
